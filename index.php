<!DOCTYPE html>
<html lang="ua">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Кавер група REMAKE завжди хороший настрій та незабутні враження від живого виконання на вашому святі.">
		<meta name="author" content="IlnytskySerhiy">
		<link rel="icon" href="./favicon.ico">

		<!-- SEO-->

		
		<!-- Favicon -->
		<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192" href="favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
		<link rel="manifest" href="favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">


		<title>Remake band</title>

		<!-- Bootstrapc grid CSS -->
		<link href="css/bootstrap-grid.min.css" rel="stylesheet">
		<link href="css/bootstrap-grid.css" rel="stylesheet">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&amp;subset=cyrillic" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/slidePage.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/css/swiper.min.css">
		<link type="text/css" rel="stylesheet" href="css/lightslider.min.css" />

		<!-- Custom styles for this template -->
		<link href="css/main.css" rel="stylesheet">
	</head>

	<body>
		
  <div id="scene" class="scene unselectable">
  	<div data-depth="1.0" class="layer">
	<svg class="background" width="965px" height="204px" viewBox="0 0 965 204" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <!-- Generator: Sketch 50 (54983) - http://www.bohemiancoding.com/sketch -->

    <g id="ffff" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" fill-opacity="0.365942029">
        <g id="Desktop-HD" transform="translate(-88.000000, -410.000000)" fill="#EDEDED">
            <g id="Group-2" transform="translate(88.000000, 410.000000)">
                <path d="M0,200.607692 L0,60.3923077 L34.0298507,60.3923077 L36.8656716,84.9865385 L38,84.9865385 C43.1045031,75.3749519 49.2487203,68.3077149 56.4328358,63.7846154 C63.6169513,59.2615158 70.8009591,57 77.9850746,57 C81.9552437,57 85.2164051,57.2355746 87.7686567,57.7067308 C90.3209083,58.177887 92.6368055,58.8846107 94.7164179,59.8269231 L87.9104478,95.7288462 C85.2636684,94.9749962 82.8059815,94.4096173 80.5373134,94.0326923 C78.2686454,93.6557673 75.6219057,93.4673077 72.5970149,93.4673077 C67.3034561,93.4673077 61.7736607,95.3519042 56.0074627,99.1211538 C50.2412647,102.890403 45.4676806,109.580721 41.6865672,119.192308 L41.6865672,200.607692 L0,200.607692 Z M172.985075,204 C162.965124,204 153.607009,202.350978 144.910448,199.052885 C136.213887,195.754791 128.651773,190.94907 122.223881,184.635577 C115.795988,178.322084 110.786088,170.642353 107.19403,161.596154 C103.601972,152.549955 101.80597,142.184674 101.80597,130.5 C101.80597,119.192251 103.696499,108.968315 107.477612,99.8278846 C111.258725,90.6874543 116.221362,82.9606085 122.365672,76.6471154 C128.509981,70.3336223 135.552199,65.4807862 143.492537,62.0884615 C151.432876,58.6961369 159.656674,57 168.164179,57 C178.18413,57 186.927823,58.6961369 194.395522,62.0884615 C201.863221,65.4807862 208.101965,70.1922775 213.11194,76.2230769 C218.121916,82.2538763 221.855709,89.3682282 224.313433,97.5663462 C226.771157,105.764464 228,114.669183 228,124.280769 C228,128.050019 227.810947,131.630752 227.432836,135.023077 C227.054724,138.415402 226.676619,140.959607 226.298507,142.655769 L142.074627,142.655769 C143.965184,152.832743 148.171609,160.324014 154.69403,165.129808 C161.216451,169.935601 169.203933,172.338462 178.656716,172.338462 C188.676667,172.338462 198.790994,169.228877 209,163.009615 L222.895522,188.169231 C215.711407,193.069255 207.723925,196.932678 198.932836,199.759615 C190.141747,202.586553 181.49258,204 172.985075,204 Z M141.791045,115.234615 L192.552239,115.234615 C192.552239,107.319191 190.756237,100.911563 187.164179,96.0115385 C183.572121,91.111514 177.522431,88.6615385 169.014925,88.6615385 C162.397977,88.6615385 156.584602,90.8288245 151.574627,95.1634615 C146.564652,99.4980986 143.30349,106.188416 141.791045,115.234615 Z" id="re"></path>
                <path d="M307,197.617111 L307,57.7910238 L341.034483,57.7910238 L343.87069,75.8330996 L345.005172,75.8330996 C350.866696,70.0069835 357.106289,64.9796845 363.724138,60.7510519 C370.341987,56.5224192 378.283287,54.4081346 387.548276,54.4081346 C397.56959,54.4081346 405.652699,56.428451 411.797845,60.4691445 C417.94299,64.5098379 422.811764,70.2888825 426.40431,77.8064516 C432.643997,71.4165178 439.214477,65.9193779 446.115948,61.3148668 C453.01742,56.7103557 461.195068,54.4081346 470.649138,54.4081346 C485.77565,54.4081346 496.884016,59.4354336 503.974569,69.4901823 C511.065122,79.5449311 514.610345,93.3112731 514.610345,110.789621 L514.610345,197.617111 L472.918103,197.617111 L472.918103,116.145863 C472.918103,105.997144 471.547284,99.0434971 468.805603,95.2847125 C466.063923,91.5259279 461.667846,89.6465638 455.617241,89.6465638 C448.621229,89.6465638 440.58539,94.1570376 431.509483,103.178121 L431.509483,197.617111 L389.817241,197.617111 L389.817241,116.145863 C389.817241,105.997144 388.446422,99.0434971 385.704741,95.2847125 C382.963061,91.5259279 378.566984,89.6465638 372.516379,89.6465638 C365.331286,89.6465638 357.389986,94.1570376 348.692241,103.178121 L348.692241,197.617111 L307,197.617111 Z M585.515517,201 C579.086749,201 573.272583,199.919366 568.072845,197.758065 C562.873106,195.596763 558.477029,192.589781 554.884483,188.737027 C551.291936,184.884272 548.503027,180.420783 546.517672,175.346424 C544.532318,170.272064 543.539655,164.72794 543.539655,158.713885 C543.539655,144.054625 549.779248,132.590504 562.258621,124.321178 C574.737993,116.051852 594.874861,110.507728 622.669828,107.68864 C622.291665,101.486645 620.448149,96.5533143 617.139224,92.8884993 C613.8303,89.2236843 608.299751,87.3913043 600.547414,87.3913043 C594.496809,87.3913043 588.493564,88.5189228 582.5375,90.7741935 C576.581436,93.0294643 570.200034,96.130415 563.393103,100.077139 L548.361207,72.7321178 C557.437114,67.2818802 566.938312,62.8653746 576.865086,59.4824684 C586.79186,56.0995623 597.33299,54.4081346 608.488793,54.4081346 C626.640608,54.4081346 640.490613,59.5294018 650.039224,69.7720898 C659.587835,80.0147777 664.362069,95.9423884 664.362069,117.5554 L664.362069,197.617111 L630.327586,197.617111 L627.491379,183.239832 L626.356897,183.239832 C620.306292,188.50213 613.97216,192.777683 607.35431,196.06662 C600.736461,199.355556 593.456936,201 585.515517,201 Z M599.696552,168.862553 C604.423587,168.862553 608.488776,167.828902 611.892241,165.761571 C615.295707,163.694239 618.888199,160.781225 622.669828,157.02244 L622.669828,132.496494 C607.921478,134.563825 597.711235,137.570808 592.038793,141.517532 C586.366351,145.464255 583.530172,150.162666 583.530172,155.612903 C583.530172,160.123445 584.995531,163.459316 587.926293,165.620617 C590.857055,167.781918 594.780435,168.862553 599.696552,168.862553 Z M699.531034,197.617111 L699.531034,0 L740.088793,0 L740.088793,113.326788 L741.223276,113.326788 L785.751724,57.7910238 L831.131034,57.7910238 L781.781034,115.30014 L834.818103,197.617111 L789.722414,197.617111 L757.956897,143.208976 L740.088793,163.224404 L740.088793,197.617111 L699.531034,197.617111 Z M909.977586,201 C899.956272,201 890.596883,199.355556 881.899138,196.06662 C873.201393,192.777683 865.638251,187.985305 859.209483,181.689341 C852.780715,175.393377 847.770133,167.734968 844.177586,158.713885 C840.58504,149.692802 838.788793,139.356299 838.788793,127.704067 C838.788793,116.427714 840.679579,106.232163 844.461207,97.1171108 C848.242835,88.0020582 853.206147,80.2966654 859.351293,74.0007013 C865.496439,67.7047371 872.539615,62.8653746 880.481034,59.4824684 C888.422453,56.0995623 896.647371,54.4081346 905.156034,54.4081346 C915.177349,54.4081346 923.922233,56.0995623 931.390948,59.4824684 C938.859664,62.8653746 945.099257,67.5637848 950.109914,73.5778401 C955.120571,79.5918954 958.854873,86.6864949 961.312931,94.8618513 C963.770989,103.037208 965,111.917203 965,121.502104 C965,125.260888 964.810921,128.83168 964.432759,132.214586 C964.054596,135.597492 963.676439,138.134634 963.298276,139.826087 L879.062931,139.826087 C880.953745,149.974805 885.160743,157.445278 891.684052,162.237728 C898.20736,167.030178 906.19593,169.426367 915.65,169.426367 C925.671314,169.426367 935.787018,166.325417 945.997414,160.123422 L959.894828,185.213184 C952.709734,190.099604 944.721165,193.9523 935.928879,196.771388 C927.136594,199.590477 918.486249,201 909.977586,201 Z M878.77931,112.481066 L929.547414,112.481066 C929.547414,104.587618 927.751167,98.1977804 924.158621,93.3113604 C920.566074,88.4249405 914.51556,85.9817672 906.006897,85.9817672 C899.389047,85.9817672 893.574881,88.1430359 888.564224,92.4656381 C883.553567,96.7882404 880.291962,103.459983 878.77931,112.481066 Z" id="make"></path>
            </g>
        </g>
    </g>
</svg>
</div>
</div>
		<div class="slide-container" id="slidePage-container">
			
			<div class="slide-page page1 transition">
				<div class="container p-0 fill" id="1">
					<div class="row no-gutters firstpage_row1">
						<div class="col-xl-3 col-lg-3 col-md-3 col-sm-5 col-logo" >
							<div class="row no-gutters logo_row">
								<!-- /logo -->
								<div class="col-xl col-lg col-md col-sm-5 border-left-0">
									<img src="img/logo.svg" class="logo" id="logoID" alt="" title="Кавер гурт ремейк">
								</div>
								<div class="col-xl col-lg-hidden col-md-hidden"></div>
							</div>
							<!-- /.row -->
							<div class="row">
								<!-- /line under logo-->
								<div class="line_logo line" style="margin-top: 25%">
								</div>
								<!-- /.row -->
							</div>
						</div>
						<!-- /.col-xl -->
						<div class="col-xl col-lg col-md d-none d-md-block col-logo">.
						</div>
						<div class="col-xl-6 col-lg col-md d-sm-none d-md-block facebooknewsbox" >
							<div class="row facebooknewstextbox medium_text" style="overflow-x: hidden">
								https://soundcloud.com/remakecover
							</div>
							<!-- /.row -->
							<div class="row text-center">
								<div class="col-xl col-lg col-md">
									<div class="custombutton">
										<div class="button_text">
											<a href="https://www.facebook.com/remake.coverband/posts/1967863746619220" target="_blank">ДЕТАЛЬНІШЕ</a>
										</div>	
									</div>
									<!-- ai?iaeoe -->
								</div>
								<!-- /.c -->
							</div>
							<!-- /.row -->
						</div>
						
						<div class="col-xl-6 col-lg-5 col-md-6">
						<!-- /.col-xl -->
						<div class="row">
						<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12" >
							<!--menu 1-->
							<div class="text menu menu_text" >
								<div onclick="slideToPage('2')" class="move_text">що нового</div>
								<div onclick="slideToPage('3')" class="move_text">про нас</div>
							</div>
						</div>
						<!-- /.col -->
						<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12">
							<!--menu 2-->
							<div class="text menu2 menu_text">
								<div onclick="slideToPage('5')" class="move_text">репертуар</div>
								<div onclick="slideToPage('6')" class="move_text">медіа</div>
								<div onclick="slideToPage('8')" class="move_text">контакти</div>
						
							</div>
						</div>
						</div>
						</div>
					
					
					<!-- /.col -->	
					<!--photo-->
					<div class="col-xl-6 col-lg-7 col-md-6 col-sm-12 order-sm-first order-md-0 div_photo_band" >
						<img src="img/foto band.jpg" class="photo-band" alt="Кавер гурт ремейк Львів" title="Кавер гурт ремейк фото учасників">
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
				<div class="row firstpage_row2">
					<div class="col-xl col-lg col-md col-sm-9 buben " >
						<!--buben-->
						<img src="img/booben.png" class="img-fluid buben" height="100%" alt="" title="Кавер гурт ремейк">
					</div>
					<!-- /.col -->
					<div class="col-xl-1 col-lg-1 col-md-1 col-sm-2" onclick="slidepage.slideNext()">
						<img src="img/arrow down.svg" class="arrow_down" alt="" title="Наступна сторінка">
					</div>
					<div class="col-xl col-lg col-md">
						
					</div>
					<!-- /.col -->
			</div>
		</div>
		<!-- /.container -->
	</div>

	<div class="slide-page page2 ">

				<div class="" style="position: absolute; margin-top: 3%; height: 5%; width: 5%; right: 12%">
							<input type="checkbox" id="hmt" class="hidden-menu-ticker">
								<label class="btn-menu" for="hmt">
 								<span class="first"></span>
 								<span class="second"></span>
 								<span class="third"></span>
								</label>

								<ul class="hidden-menu">
 								<br><br><br>
 								<div class="text menu menu_text">
									<div onclick="slideToPage('2')" class="move_text">що нового</div>
						
									<div onclick="slideToPage('3')" class="move_text">про нас</div>

									<div onclick="slideToPage('5')" class="move_text">репертуар</div>
									<div onclick="slideToPage('6')" class="move_text">медіа</div>
									<div onclick="slideToPage('8')" class="move_text">контакти</div>
								</div>

								</ul>
								<div class="fader" id="fader">	</div>
								<script>
									fader.onclick = function() {
										console.log('hide menu')
    									document.getElementById('hmt').checked = false;
									};
								</script>
								
    		</div>


    <div class="container p-0 fill" id="page2">
    	<div class="row secondpage_row1 " id="2">
    		<div class="col-xl-7 col-lg-7 col-md-9 col-sm-9">
    			<div class="title">
    			що нового
    			</div>
    		</div>
    		<!-- /.col-xl -->
    		<div class="col-xl col-lg col-md col-sm"></div>
    		<!-- /.col-xl -->
    
    		<!-- /.col-xl -->
    	</div>
    	<!-- /.row -->
    	<div class="row secondpage_row2">
    		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
    			<div class="line2"></div>
    		</div>
    		<!-- /.col-xl-12 -->
    	</div>
    	<!-- /.row -->

			<div class="row secondpage_row3">
				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
					<div class="linevertical" >
						
					</div>
				</div>
				<div class="col-xl col-lg col-md col-sm">	
					<div class="newstext">
						<a href="https://www.facebook.com/remake.coverband/posts/1967863746619220" class="fcb_text" target="_blank">
						https://soundcloud.com/remakecover</a>
					</div>

				</div>
			</div>
			<div class="row secondpage_row4">
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 order-sm-last order-md-0">
					<img src="img/pic.png" srcset="img/pic@2x.png 2x, img/pic@3x.png 3x" class="pic" alt="" title="Кавер гурт ремейк">
				</div>
				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
					<div class="linevertical" >
						
					</div>
				</div>
				<div class="col-xl col-lg col-md col-sm" style="margin: auto;">
					<div class="newstext">
						<a href="https://www.facebook.com/remake.coverband/posts/1955977447807850" class="fcb_text" target="_blank">
						Ще трішки фото з вчорашнього весілля. @ Ресторан Ратуша</a>
					</div>
				</div>
			</div>

    </div>  
    </div>


    <div class="slide-page page3">
    	<div class="" style="position: absolute; margin-top: 3%; height: 5%; width: 5%; right: 12%">
							<input type="checkbox" id="hmt1" class="hidden-menu-ticker">
								<label class="btn-menu" for="hmt1">
 								<span class="first"></span>
 								<span class="second"></span>
 								<span class="third"></span>
								</label>

								<ul class="hidden-menu">
 								<br><br><br>
 								<div class="text menu menu_text">
									<div onclick="slideToPage('2')" class="move_text">що нового</div>
						
									<div onclick="slideToPage('3')" class="move_text">про нас</div>

									<div onclick="slideToPage('5')" class="move_text">репертуар</div>
									<div onclick="slideToPage('6')" class="move_text">медіа</div>
									<div onclick="slideToPage('8')" class="move_text">контакти</div>
								</div>

								</ul>
								<div class="fader" id="fader1">	</div>
								<script>
									fader1.onclick = function() {
										console.log('hide menu')
    									document.getElementById('hmt1').checked = false;
									};
								</script>
								
    		</div>
 		<div class="container p-0 ">
    	<div class="row thirdpage_row1" id="3">
    		<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">
    			<div class="title">
    			про нас
    			</div>
    		</div>
    		<!-- /.col-xl -->
    		<div class="col-xl col-lg col-md col-sm"></div>
    		<!-- /.col-xl -->

    	</div>
    	<!-- /.row -->
			<div class="row thirdpage_row2" >
				<div class="col-xl-4 col-lg-3 col-md-3 col-sm-12">
					<div class="discription_text move_left" id="discription_text" >
						Кавер група REMAKE завжди хороший настрій та незабутні враження від живого виконання на вашому святі. 
						<div class="line" style="margin-top:15%; margin-left: 40%"></div>
					</div>
				 
				</div>
				<div class="col-xl-8 col-lg-9 col-md-9 col-sm-12 order-sm-first order-md-0 ph-block" style="text-align: right !important; display: flex; justify-content: center; ">
					<img src="img/foto-band.png" srcset="img/foto-band@2x.png 2x, img/foto-band@3x.png 3x" class="photo-band" id="showPhoto1" alt="Кавер гурт ремейк" title="Кавер гурт ремейк, фото учасників">

				</div>
			</div>
			<!-- /.row -->

			<div class="row thirdpage_row3" style="max-height: 100%; overflow: hidden">
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6" style="max-height: 100%">
					<div class="row about_row1">
						<div class="col-xl-12 col-lg col-md col-sm" style="position: relative;">
							<div class="move_left_slowest" id="icon1" style="position: absolute; height: 100%; margin-left:-15px;">
								<img src="img/icon-1.svg" class="icon" alt="" title="Живий звук">
							</div>
						</div>
					</div>
					<!-- /.row -->
					
					<div class="row about_row2" >
						<div class="col-xl-12 col-lg col-md col-sm about_titles_text" style="position: relative;">
							<div class="move_bottom_slow" id="move_bottom1" style="left: 0%">
								ЖИВИЙ ЗВУК
							</div>
						</div>
					</div>
					<!-- /.row -->
					<div class="row about_row3">
						<div class="col-xl-12 col-lg col-md col-sm about_discriptions_text">
							<div class="move_bottom_slow" id="move_bottom11" style="left: 0%">
								Не використовуємо фонограми чи плей-беки
							</div>
						</div>
					</div>
					
					<!-- /.row -->
				</div>
				<!-- /.col-xl-3 -->
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6" style="max-height: 100%">
					<div class="row about_row1">
						<div class="col-xl-12 col-lg col-md col-sm" style="position: relative;">
							<div class="move_left_slower" id="icon2" style="position: absolute; height: 100%; margin-left:-15px;">
								<img src="img/icon-2.svg" class="icon" alt="" title="Професійні інструменти">

							</div>
						</div>
					</div>
					<!-- /.row -->
					
					<div class="row about_row2">
							<div class="col-xl-12 col-lg col-md col-sm about_titles_text">
								<div class="move_bottom" id="move_bottom2" style="left: 0%">
								ПРОФЕСІЙНІ ІНСТРУМЕНТИ
								</div>
							</div>
					</div>
					<!-- /.row -->
					<div class="row about_row3">
							<div class="col-xl-12 col-lg col-md col-sm about_discriptions_text">
								<div class="move_bottom" id="move_bottom22" style="left: 0%">
							Ударні, бас-гітара, електро-гітара, клавішні, акустична гітара
								</div>
							</div>
					</div>

				</div>
					<!-- /.row -->

				<!-- /.col-xl-3 -->
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6" style="max-height: 100%">
					<div class="row about_row1">
						<div class="col-xl-12 col-lg col-md col-sm" style="position: relative;">
							<div class="move_right_slower" id="icon3" style="position: absolute; height: 100%; margin-left:-15px;">
								<img src="img/icon-3.svg" class="icon" alt="" title="Сучасні хіти">
							</div>
						</div>
					</div>
					<!-- /.row -->
					
					<div class="row about_row2">
						<div class="col-xl-12 col-lg col-sm col-md about_titles_text">
							<div class="move_bottom_fast" id="move_bottom3" style="left: 0% ">
							СУЧАСНІ ХІТИ
							</div>
						</div>
					</div>
					<!-- /.row -->
					<div class="row about_row3">
						<div class="col-xl-12 col-lg col-md col-sm about_discriptions_text">
							<div class="move_bottom_fast" id="move_bottom33" style="left: 0% ">
								Виконуємо як і традиційні композиції так і сучасні відомі хіти
								</div>
						</div>
					</div>
				</div>
					<!-- /.row -->
				
				<!-- /.col-xl-3 -->
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6" style=" max-height: 100%">
					<div class="row about_row1">
						<div class="col-xl-12 col-lg col-md col-sm" style="position: relative;">
							<div class="move_right_slowest" id="icon4" style="position: absolute; height: 100%; margin-left:-15px;">
								<img src="img/icon-4.svg" class="icon" alt="" title="Якісне виконання">
							</div>
						</div>
					</div>
					<!-- /.row -->
					
					<div class="row about_row2">
						<div class="col-xl-12 col-lg col-md col-sm about_titles_text">
							<div class="move_bottom_faster" id="move_bottom4" style="left: 0%">
							ЯКІСНЕ ВИКОНАННЯ
							</div>
						</div>
					</div>
					<!-- /.row -->
					<div class="row about_row3">
						<div class="col-xl-12 col-lg col-md col-sm about_discriptions_text">
							<div class="move_bottom_faster" id="move_bottom44" style="left: 0%">
							В колективі Remake Cover Band професійні музиканти та виконавці
						</div>
						</div>
					</div>
				</div>
					<!-- /.row -->
				
				<!-- /.col-xl-3 -->


			</div>
			<!-- /.row -->





    </div>  

	  </div>	
<div class="slide-page page4" >
	<div class="" style="position: absolute; margin-top: 3%; height: 5%; width: 5%; right: 12%">
							<input type="checkbox" id="hmt2" class="hidden-menu-ticker">
								<label class="btn-menu" for="hmt2">
 								<span class="first"></span>
 								<span class="second"></span>
 								<span class="third"></span>
								</label>

								<ul class="hidden-menu">
 								<br><br><br>
 								<div class="text menu menu_text">
									<div onclick="slideToPage('2')" class="move_text">що нового</div>
						
									<div onclick="slideToPage('3')" class="move_text">про нас</div>

									<div onclick="slideToPage('5')" class="move_text">репертуар</div>
									<div onclick="slideToPage('6')" class="move_text">медіа</div>
									<div onclick="slideToPage('8')" class="move_text">контакти</div>
								</div>

								</ul>
								<div class="fader" id="fader2">	</div>
								<script>
									fader2.onclick = function() {
										console.log('hide menu')
    									document.getElementById('hmt2').checked = false;
									};
								</script>
								
    		</div>
<div class="container p-0 " id="4">
			


			<div class="row fourpage_row1">
    		<div class="col-xl-7 col-lg-7 col-md-7 col-sm-7">
    			<div class="title">
    			колектив
    			</div>
    		</div>
 				
    		

			</div>
			<!-- /.row -->
			<div class="row fourpage_row2 members" style="position: relative; display: flex; align-items: center; justify-content: center; overflow: hidden" >
				
				
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 members1" >
					
					<img src="img/bitmap_5.png" srcset="img/bitmap@2x.png 2x, img/bitmap@3x.png 3x"
   				class="member_photo" alt="Учасник кавер гурту Ремейк" title="Учасник гурту Ремейк, Вадим">
   				
   				<div class="about_titles hide_member_info" >
   					ВАДИМ
					</div>
					<div class="about_discriptions hide_member_info" >
						Акустична гітара, вокал
					</div>
				

				</div>

				<div class="member1_discription" style="position: absolute">
					<div class="row">	
					<div class="col-2">
						<div class="linevertical" >
						</div>
					</div>
					<div class="col">
						<div class="name_title">ВАДИМ</div>
						<div class="instruments_title">Акустична гітара, вокал</div> 
						<div class="member_discription">
				Справжній магістр класичної гітари. Лауреат регіонального конкурсу таланти рідного краю2000р, дипломант міжнародного конкурсу ім. Джона Дюарта 2002. Дипломант всеукраїнського конкурсу гітаристів 2006р. Учасник гітарних фестивалів в м.Нюрнберг(Німеччина), Краків (Польща), Руст(Австрія), Брно(Чехія). Виступав з концертами в Німеччині, Іспанії, Польщі. Засновник колективу "Акустичний проект "ТРИ Гітари", з яким в 2007 році став лауреатом міжнародного конкурсу та володарем Гран Прі. В його репертуарі неперевершене виконання пісень Висоцького, Сукачова, гр. ДДТ, Чайф.
				</div>
				</div>

					</div>

				</div>
	

				<!-- /.col-xl-4 -->
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 members2" >
					<img src="img/bitmap.png" srcset="img/bitmap@2x.png 2x, img/bitmap@3x.png 3x"
   				class="member_photo" alt="Учасник кавер гурту Ремейк" title="Учасник гурту Ремейк, Валерія">
   				<div class="about_titles hide_member_info" >
   					ВАЛЕРІЯ
					</div>
					<div class="about_discriptions hide_member_info">
						Вокал
					</div>

				</div>

				<div class="member2_discription" style="position: absolute">
					<div class="row">	
					<div class="col-2">
						<div class="linevertical" >
						</div>
					</div>
					<div class="col">
						<div class="name_title">ВАЛЕРІЯ</div>
						<div class="instruments_title">Вокал</div> 
						<div class="member_discription">
				Перлина групи. Лауреат співочих конкурсів «Молода Галичина», «Мелодія», «Веселад», «Золотий тік». Виступала як співачка в багатьох країнах світу. Справжній майстер перевтілення голосу. Підкорює вокальні партії від лірики Селін Діон до драйву AC/DC, від народних пісень до французького шансону Едіт Піаф та ін…
				</div>
				</div>

					</div>

				</div>

				<!-- /.col-xl-4 -->
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 members3">
					<img src="img/bitmap_1.png" srcset="img/bitmap@2x.png 2x, img/bitmap@3x.png 3x"
   				class="member_photo" alt="Учасник кавер гурту Ремейк" title="Учасник гурту Ремейк, Юрій">
   				<div class="about_titles hide_member_info">
   					ЮРІЙ
					</div>
					<div class="about_discriptions hide_member_info">
						Клавішні, вокал
					</div>


				</div>
				<!-- /.col-xl-4 -->
			
				<div class="member3_discription" style="position: absolute">
					<div class="row">	
					<div class="col-2">
						<div class="linevertical" >
						</div>
					</div>
					<div class="col">
						<div class="name_title">ЮРІЙ</div>
						<div class="instruments_title">Клавішні, вокал</div>
						<div class="member_discription">
				Освіта вища. Працював з Т. Кукурузою, І. Мацялком, О. Кульчицьким, І. Богданом. Співпрацює з П. Табаковим та групою "БлюзМаркет". Також є засновником студії "Fonia records"
				</div>
				</div>

					</div>

				</div>


				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 members4">
					<img src="img/bitmap_2.png" srcset="img/bitmap@2x.png 2x, img/bitmap@3x.png 3x"
   				class="member_photo" alt="Учасник кавер гурту Ремейк" title="Учасник гурту Ремейк, Ігор">
   				<div class="about_titles hide_member_info">
   					ІГОР
					</div>
					<div class="about_discriptions hide_member_info">
						Бас-гітара, вокал
					</div>

				</div>

				<div class="member4_discription" style="position: absolute">
					<div class="row">	
					<div class="col-2">
						<div class="linevertical" >
						</div>
					</div>
					<div class="col">
						<div class="name_title">ІГОР</div>
						<div class="instruments_title">Бас-гітара, вокал</div>
						<div class="member_discription">
				Засновник та керівник групи, займається організацією виступів. За освітою трубач. Кар’єру бас гітариста розпочинав в групі «Лесик Бенд». В складі різних колективів гастролював в Німеччині, Росії, Польщі, Ізраїлі, Люксембурзі. Виступав як сольний співак в різних клубах з програмою італійської музики. Також в репертуарі є пісні різних стилів і жанрів (рок-класика, поп-музика, диско 80-х…).
				</div>
				</div>

					</div>

				</div>

				<!-- /.col-xl-4 -->
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 members5">
					<img src="img/bitmap_3.png" srcset="img/bitmap@2x.png 2x, img/bitmap@3x.png 3x"
					class="member_photo" alt="Учасник кавер гурту Ремейк" title="Учасник гурту Ремейк, Віталій">
					<div class="about_titles hide_member_info" >
   					ВІТАЛІЙ
					</div>
					<div class="about_discriptions hide_member_info">
						Ударні
					</div>

				</div>

				<div class="member5_discription" style="position: absolute">
					<div class="row">	
					<div class="col-2">
						<div class="linevertical" >
						</div>
					</div>
					<div class="col">
						<div class="name_title">ВІТАЛІЙ</div>
						<div class="instruments_title">Ударні</div>
						<div class="member_discription">
				Пульс групи. Музикознавець за фахом, підкорив професію звукорежисера, і вважається одним з найкращих звукорежисерів у Львові. Озвучував багатьох українських і світових зірок. Також є одним з найкращих барабанщиків Львова. Співпрацює з Павлом Табаковим, групою «Кожному своє» та іншими співаками та музикантами.
				</div>
				</div>

					</div>

				</div>

				<!-- /.col-xl-4 -->
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 members6">
					<img src="img/bitmap_4.png" srcset="img/bitmap@2x.png 2x, img/bitmap@3x.png 3x" class="member_photo" alt="Учасник кавер гурту Ремейк" title="Учасник гурту Ремейк, Володимир">
					<div class="about_titles hide_member_info" >
   					ВОЛОДИМИР
					</div>
					<div class="about_discriptions hide_member_info">
						Електрогітара, вокал
					</div>
					
				</div>

				<div class="member6_discription" style="position: absolute">
					<div class="row">	
					<div class="col-2">
						<div class="linevertical" >
						</div>
					</div>
					<div class="col">
						<div class="name_title">ВОЛОДИМИР</div>
						<div class="instruments_title">Електрогітра, вокал</div>
						<div class="member_discription">
				Епіцентр драйву і енергетики групи. Його креативні гітарні соло з використанням стаканчиків та інших предметів зворушують навіть найспокійніших слухачів. Він є учасником групи „Кожному своє”. Також співпрацював з Павлом Табаковим, та іншими музикантами і групами. В 2006 р. він став „Найкращим гітаристом” (таке звання йому дали на фестивалі „Тарас Бульба”).
				</div>
				</div>

					</div>

				</div>
				<!-- /.col-xl-4 -->
			</div>
			<!-- /.row -->






    </div> 
    </div>
    <div class="slide-page page5"> 
    <div class="" style="position: absolute; margin-top: 3%; height: 5%; width: 5%; right: 8%">
							<input type="checkbox" id="hmt3" class="hidden-menu-ticker">
								<label class="btn-menu" for="hmt3">
 								<span class="first"></span>
 								<span class="second"></span>
 								<span class="third"></span>
								</label>

								<ul class="hidden-menu">
 								<br><br><br>
 								<div class="text menu menu_text">
									<div onclick="slideToPage('2')" class="move_text">що нового</div>
						
									<div onclick="slideToPage('3')" class="move_text">про нас</div>

									<div onclick="slideToPage('5')" class="move_text">репертуар</div>
									<div onclick="slideToPage('6')" class="move_text">медіа</div>
									<div onclick="slideToPage('8')" class="move_text">контакти</div>
								</div>

								</ul>
								<div class="fader" id="fader3">	</div>
								<script>
									fader3.onclick = function() {
										console.log('hide menu')
    									document.getElementById('hmt3').checked = false;
									};
								</script>
								
    		</div> 
			<div class="container p-0 ">
    	<div class="row fivepage_row1" id="5">
    		<div class="col-xl-6 col-lg-6 col-md-5 col-sm-7">
    			<div class="div_title">
    				<div class="title">	
    		  		репертуар
    		  	</div>
    			</div>
    		</div>
    		<div class="col-xl-5 col-lg-5 col-md-6" style="margin-top: 1%" >

							<div class="repertuar" style="text-align: right">
								<div onclick="swiper2.slideTo(1, 2000, false);">українська</div>
							</div>
							<div class="repertuar" style="text-align: right">
								<div onclick="swiper2.slideTo(2, 2000, false);">зарубіжна</div>
							</div>

							<div class="repertuar" style="text-align: right">
								<div onclick="swiper2.slideTo(3, 2000, false);">останні новинки</div>
							</div>
							<div class="repertuar" style="text-align: right">
								<div onclick="swiper2.slideTo(4, 2000, false);">послухати</div>
							</div>
							
							
				</div>
    		
    	</div>
 
			<div class="row fivepage_row2">
				<div class="col-xl-2 col-lg-2 col-md-2 col-sm-4" style="height: 100%"> <!-- Caa?aoe eeoi?-->
					<img src="img/foto.png" srcset="img/foto@2x.png 2x, img/foto@3x.png 3x" class="foto"  alt="" title="Кавер гурт Ремейк">

				</div>
				<!-- /.col-xl- -->
				<div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 order-md-0 order-sm-first"  style="height: 100%">

					<!-- /.row -->
					<div class="row" style="height: 90%">
						<div class="col-xl-12 col-lg-12 col-md-12" style="height: 100%; overflow-x: hidden;">
							<div class="swiper-container2">
								<div class="swiper-wrapper">

									<div class="swiper-slide">
										<div class ="repertuar_container" style="overflow-x: hidden;">
											<div class="row" >
												
										<?php
										$handle = fopen("data/ukraine.txt", "r");
										if ($handle) {
    									while (($line = fgets($handle)) !== false) {
     									   // process the line read. 
    										echo '<div class="col-6">';
    										echo $line;
    										echo '</div>';


    									}

   											 fclose($handle);
											} else {
   									 			// error opening the file.
											} 
										  ?>
										  </div>
										  </div>	
									</div>
									<div class="swiper-slide" >
											<div class ="repertuar_container"  style="overflow-x: hidden;">
											<div class="row">
												
										<?php
										$handle = fopen("data/foreign.txt", "r");
										if ($handle) {
    									while (($line = fgets($handle)) !== false) {
     									   // process the line read. 
    										echo '<div class="col-6">';
    										echo $line;
    										echo '</div>';


    									}

   											 fclose($handle);
											} else {
   									 			// error opening the file.
											} 
										  ?>
										  </div>
										  </div>
									</div>
									<div class="swiper-slide" >
									<div class ="repertuar_container"  style="overflow-x: hidden;">
											<div class="row">
												
										<?php
										$handle = fopen("data/last.txt", "r");
										if ($handle) {
    									while (($line = fgets($handle)) !== false) {
     									   // process the line read. 
    										echo '<div class="col-6">';
    										echo $line;
    										echo '</div>';


    									}

   											 fclose($handle);
											} else {
   									 			// error opening the file.
											} 
										  ?>
										  </div>
										  </div>
									</div>
									
									<div class="swiper-slide" >

										<div class="music music_body " style="height: 100%">
											<div class="music_container">
												<div class="music_column add-bottom" style="height: 100%; text-align: left !important;">
													<div id="mainwrap" style="height: 100%">
														<div id="nowPlay" >
															<span class="left" id="npAction">Paused...</span>
															<span class="right" id="npTitle"></span>
														</div>
														<div id="audiowrap">
															<div id="audio0">
																<audio preload id="audio1" controls="controls">Ваш браузер не підтрумує відтворення аудіо файлів</audio>
															</div>
															<div id="tracks">
																<a id="btnPrev">&larr;</a>
																<a id="btnNext">&rarr;</a>
															</div>
														</div>
														<div id="plwrap" style="height: 80%; overflow: auto; text-align: left !important">
															<ul id="plList"></ul>
														</div>
													</div>
												</div>
												<div class="column add-bottom center">
												</div>
											</div>
										</div>

									</div>
									<!-- /.col-xl-6 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.col-xl- -->
						</div>
						<!-- /.row -->
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="slide-page page6">
    <div class="container p-0 ">
    	<div class="row sixpage_row1" id="6">
    		<div class="col-xl col-lg col-md">
    			
    		</div>
    		<div class="col-xl col-lg col-md">
    			<div class="title" style="text-align: right">
    			медіа
    			</div>
    		</div>
    	</div>
 
			<div class="row sixpage_row2">
				<div class="col-xl-3 col-lg-3 col-md-3 title_little_box">
					<div class=" title_little little_title_text">ВІДЕО</div>
				</div>
			</div>

			<div class="row sixpage_row3">
				<div class="col-xl-8 col-8">

					<div class="row">
						<div class="col-xl-12">		
						<div class="video" id="video" style="">		

							<div id="player" style="width: 100%; max-height:100%; background: white"></div>

						</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-12">
							
						<div class="panel">


							<div id="back" onclick="player.previousVideo();" style="margin-top: 1%">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="29.717747001090522 437.4657808069793 129.0050004185415 110.77800000000002" class="video_controls" style="width: 23.41px; height: 20px">
									<defs>
										<path d="M30.72 453.55C30.72 461.21 30.72 522.5 30.72 530.16C30.72 538.45 37.44 545.17 45.73 545.17C54.01 545.17 60.73 538.45 60.73 530.16C60.73 527.61 60.73 514.84 60.73 491.86C60.73 468.87 60.73 456.1 60.73 453.55C60.73 445.26 54.01 438.54 45.73 438.54C37.44 438.54 30.72 445.26 30.72 453.55Z" id="aHyUMdyK8"></path>
										<path  d="M146.33 438.47C144.71 438.47 143.08 438.89 141.63 439.72C134.01 444.12 73.05 479.32 65.43 483.72C62.52 485.4 60.73 488.5 60.73 491.85C60.73 495.21 62.52 498.31 65.43 499.99C73.05 504.39 134.01 539.59 141.63 543.98C143.08 544.82 144.71 545.24 146.33 545.24C147.95 545.24 149.57 544.82 151.03 543.98C153.93 542.31 155.72 539.2 155.72 535.85C155.72 527.05 155.72 456.66 155.72 447.86C155.72 444.5 153.93 441.4 151.03 439.72C149.57 438.89 147.95 438.47 146.33 438.47Z" id="dkCbgW6og"></path>
									</defs>
									<g><g>
										<use xlink:href="#aHyUMdyK8" opacity="1" fill="#070405" fill-opacity="1"></use>
										<g><use xlink:href="#aHyUMdyK8" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="0"></use></g></g>
										<g><use xlink:href="#dkCbgW6og" opacity="1" fill="#070405" fill-opacity="1"></use>
										<g><use xlink:href="#dkCbgW6og" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="0"></use></g></g></g></svg>
							</div>

							<div id="play" onclick="player.playVideo();" style="margin-top: 1%">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="438.68174700109057 35.460780806979265 98.99199999999996 110.779" width="17.79" height="20" class="video_controls"><defs><path d="M449.08 143.24C450.7 143.24 452.32 142.82 453.78 141.98C461.4 137.58 522.36 102.39 529.98 97.99C532.88 96.31 534.67 93.21 534.67 89.85C534.67 86.49 532.88 83.39 529.98 81.71C522.36 77.31 461.4 42.12 453.78 37.72C452.32 36.88 450.7 36.46 449.08 36.46C447.46 36.46 445.83 36.88 444.38 37.72C441.47 39.4 439.68 42.5 439.68 45.86C439.68 54.65 439.68 125.04 439.68 133.84C439.68 137.2 441.47 140.3 444.38 141.98C445.83 142.82 447.46 143.24 449.08 143.24Z" id="atyDNbGoD"></path></defs><g><g><use xlink:href="#atyDNbGoD" opacity="1" fill="#070405" fill-opacity="1"></use><g><use xlink:href="#atyDNbGoD" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="0"></use></g></g></g></svg>
							</div>

							<div id="pause" onclick="player.pauseVideo();" style="margin-top: 1%">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="250.1007470010908 35.60678080697938 79.03799999999998 110.63400000000004" width="14.08" height="20" class="video_controls"><defs><path d="M251.1 51.61C251.1 59.28 251.1 120.57 251.1 128.23C251.1 136.52 257.82 143.24 266.11 143.24C274.4 143.24 281.12 136.52 281.12 128.23C281.12 120.57 281.12 59.28 281.12 51.61C281.12 43.33 274.4 36.61 266.11 36.61C257.82 36.61 251.1 43.33 251.1 51.61Z" id="auSof5nXl"></path><path d="M296.12 51.61C296.12 59.28 296.12 120.57 296.12 128.23C296.12 136.52 302.84 143.24 311.13 143.24C319.42 143.24 326.14 136.52 326.14 128.23C326.14 120.57 326.14 59.28 326.14 51.61C326.14 43.33 319.42 36.61 311.13 36.61C302.84 36.61 296.12 43.33 296.12 51.61Z" id="e1qHO6KskW"></path></defs><g><g><use xlink:href="#auSof5nXl" opacity="1" fill="#070405" fill-opacity="1"></use><g><use xlink:href="#auSof5nXl" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="0"></use></g></g><g><use xlink:href="#e1qHO6KskW" opacity="1" fill="#070405" fill-opacity="1"></use><g><use xlink:href="#e1qHO6KskW" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="0"></use></g></g></g></svg>
							</div>

							<div id="next"  onclick="player.nextVideo();" style="margin-top: 1%">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="408.66874700109054 437.4657808069792 129.005 110.77800000000013" width="23.41" height="20" class="video_controls"><defs><path d="M504.66 453.55C504.66 456.1 504.66 468.87 504.66 491.86C504.66 514.84 504.66 527.61 504.66 530.16C504.66 538.45 511.38 545.17 519.67 545.17C527.95 545.17 534.67 538.45 534.67 530.16C534.67 522.5 534.67 461.21 534.67 453.55C534.67 445.26 527.95 438.54 519.67 438.54C511.38 438.54 504.66 445.26 504.66 453.55Z" id="b17t4LXDFL"></path><path d="M423.76 439.72C422.31 438.89 420.69 438.47 419.06 438.47C417.44 438.47 415.82 438.89 414.37 439.72C411.46 441.4 409.67 444.5 409.67 447.86C409.67 456.66 409.67 527.05 409.67 535.85C409.67 539.2 411.46 542.31 414.37 543.98C415.82 544.82 417.44 545.24 419.06 545.24C420.69 545.24 422.31 544.82 423.76 543.98C431.38 539.59 492.34 504.39 499.96 499.99C502.87 498.31 504.66 495.21 504.66 491.85C504.66 488.5 502.87 485.4 499.96 483.72C484.72 474.92 431.38 444.12 423.76 439.72Z" id="a1bcWXVLtK"></path></defs><g><g><use xlink:href="#b17t4LXDFL" opacity="1" fill="#070405" fill-opacity="1"></use><g><use xlink:href="#b17t4LXDFL" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="0"></use></g></g><g><use xlink:href="#a1bcWXVLtK" opacity="1" fill="#070405" fill-opacity="1"></use><g><use xlink:href="#a1bcWXVLtK" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="0"></use></g></g></g></svg>
							</div>

							<div id="time">00:00</div>	

							<div id="line" onclick="progress(event);">
								<div class="viewed"></div>
								<div id="videoFader"></div>
							</div>
							
							<div id="volume_line" onclick="volume(event);">
								<div class="volume_level"></div>
								<div id="volume_fader"></div>
							</div>
							
						</div>		

					</div>
					</div>
					<div class="row">
						<div class="col-xl-12">
							<div class="about_titles_text video_text" id="videotitle">
								Remake CoverBand - Різдвяні - Новорічні (2018)

							</div>
							<br>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-4 scroll" id="player_list" style="margin-top: -1%">

    	<script type="text/javascript">

    	</script>

					
				</div>
												 </symbol>	
<script type="text/javascript">

</script>
			</div>

    </div>  
    </div>
    <div class="slide-page page7">

		 <div class="container p-0 ">
 
			<div class="row page7_row1" id="7">
				<div class="col-xl-3 col-lg-3 col-md-3">
					<div class="title_little_box" style="margin-top: 6%">	
						<div class="title_little little_title_tex">
							ФОТО
						</div>
					</div>
				</div>
			</div>

			<div class="row page7_row2" >
				<div class="col-xl-12 col-lg-12 col-md-12 foto" style="margin-top: 3%">
					 <div class="swiper-container" >
  					<div class="swiper-wrapper">
   						<div class="swiper-slide">
   							<img src="img/gallary/RL1.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL2.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
							<div class="swiper-slide">
   							<img src="img/gallary/RL3.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
							<div class="swiper-slide">
   							<img src="img/gallary/RL4.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL5.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL6.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL7.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL8.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL9.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL10.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL11.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL12.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL13.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL14.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL15.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL16.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL7.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL18.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL19.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
   						<div class="swiper-slide">
   							<img src="img/gallary/RL20.jpg" class="gallary_photo" alt="Кавер гурт Ремейк" title="Кавер гурт Ремейк, репетиція">
   						</div>
  					</div>
  					<!-- Add Pagination -->
  					<div class="swiper-pagination"></div>
  					<!-- Add Arrows -->
  					<div class="swiper-button-next"></div>
  					<div class="swiper-button-prev"></div>
 					</div>

				</div>
			</div>

			<!-- <div class="row">
				<div class="col">
					<img src="img/indicator-foto.svg" class="indicator-foto " style="padding-left: 30%; margin-top: 50px">
				</div>
			</div> -->

    </div> 
    </div>
    <div class="slide-page page8">
    <div class="container p-0 " id="8">
    	<div class="row page8_row1">
    		<div class="col-xl col-lg col-md col-sm"></div>
     		<div class="col-xl col-lg col-md col-sm">
    			<div class="title" style="text-align: right">
    			контакти
    			</div>
    		</div>
    	</div>
 
			<div class="row page8_row2">
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 title_little_box">
					<div class="title_little title_little_text">ЗВ'ЯЖІТЬСЯ З НАМИ</div>
				</div>
			</div>

			<div class="row page8_row3">
				<div class="col-xl-3 col-lg-3 col-md-3 col-sm-3"></div>
				<div class="col-xl col-lg col-md col-sm">
					<div class="line" style="align"></div>
				</div>
				<div class="col-xl col-lg col-md col-sm"></div>
			</div>

			<div class="row page8_row4">
				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1">
					
				</div>
				<div class="col-xl col-lg col-md col-sm contacts_text">
					<a href="mailto:ihor-khomyn@ukr.net" class="move_text">
					ihor-khomyn@ukr.net
					</a>
					<br>
					<a href="mailto:ihor.khomyn@gmail.com" class="move_text">
					ihor.khomyn@gmail.com
					</a>
					<br>
					<div class="mt-xl-12 mt-lg-12 col-md-12 col-sm-12">
						<a href="tel:+380972633833" class="move_text">
					+38(097) 26-33-833
						</a>
					</div>
				</div>
			</div>

			<div class="row page8_row5">
				<div class="col-xl-3 col-lg-3 col-sm-3 mt-lg-5 col-md-3"></div>
				<div class="col-xl col-lg col-md col-sm">
					<div class="line"></div>
				</div>
				<div class="col-xl col-lg col-md col-sm"></div>
			</div>

			<div class="row page8_row6">
				<div class="col-xl col-lg col-md col-sm">
					<div style="margin-top: 50%">
						<a href="https://twitter.com/CoverBandRemake" target="_blank">
						<img src="img/twitter.svg" class="sotial_networks" alt="" title="Твіттер">
						</a>
					</div>
				</div>

				<div class="col-xl col-lg col-md col-sm">
					<div style="margin-top: 50%">
					<a href="https://www.facebook.com/remake.coverband" target="_blank">
						<img src="img/facebook.svg" class="sotial_networks" alt="" title="Фейсбук">
					</a>
					</div>
				</div>
				<div class="col-xl col-lg col-md col-sm">
					<div style="margin-top: 50%">
					<a href="https://www.instagram.com/remakecover/" target="_blank">
						<img src="img/instagram.svg"  class="sotial_networks" alt="" title="Інстаграм">
 					</a>
 					</div>
				</div>
				<div class="col-xl col-lg col-md col-sm">
					<div style="margin-top: 50%">
					<a href="https://www.youtube.com/user/ReMakeCover" target="_blank">
						<img src="img/youtube.svg" class="sotial_networks" alt="" title="Ютуб">
					</a>
					</div>
				</div>
				<div class="col-xl-1 col-lg-1 col-md-1 col-sm-1"></div>
				<div class="col-xl-5 col-lg-5 col-md-5 col-sm-5">
					<img src="img/foto1.png" srcset="img/foto@2x1.png 2x, img/foto@3x1.png 3x"  style="max-height: 100%; max-width: 100%" alt="" title="Кавер гурт Ремейк">
				</div>
			</div>

    </div>  
    </div>
    </div> 
		<!-- Bootstrap core JavaScript ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->

		

		<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script>window.jQuery || document.write('<script src="js/jquery-3.2.1.slim.min.js"><\/script>')</script> -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<!-- <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>			 -->
		<script type="text/javascript" src="js/slidePage.min.js"></script>
		

		<script type="text/javascript">
// Dependencies:
// https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js
// https://cdnjs.cloudflare.com/ajax/libs/html5media/1.1.8/html5media.min.js
// https://cdnjs.cloudflare.com/ajax/libs/plyr/2.0.18/plyr.js

// Inspiration: http://jonhall.info/how_to/create_a_playlist_for_html5_audio
// Mythium Archive: https://archive.org/details/mythium/


jQuery(function ($) {
  'use strict'
  var supportsAudio = !!document.createElement('audio').canPlayType;
  if (supportsAudio) {
    var index = 0,
      playing = false,
      mediaPath = '/',
      extension = '',
      tracks = [
			<?php
              $dirname = "mp3/";
              $files = scandir($dirname);
              $ignore = array(".", "..", ".DS_Store");
              $a = 1; 
              foreach($files as $curfile){
                if(!in_array($curfile, $ignore)) {

                  echo '{ "track": '.$a.', "name": "'.substr($curfile, 0, strlen($curfile) -4). '", "duration": "25:28", "file": "mp3/'.substr($curfile, 0, strlen($curfile) -4).'" }, ';
                  $a = $a + 1;
                }
              } 
            ?>
      {
        "track": 0,
        "name": "",
        "duration": "",
        "file": "JLS_ATI"
      }],
      buildPlaylist = $(tracks).each(function(key, value) {
        var trackNumber = value.track,
          trackName = value.name,
          trackDuration = value.duration;
        if (trackNumber.toString().length === 1) {
          trackNumber = '0' + trackNumber;
        }
        $('#plList').append('<li><div class="plItem"><span class="plNum">' + trackNumber + '.</span><span class="plTitle">' + trackName + '</span><span class="plLength">' + trackDuration + '</span></div></li>');
      }),
      trackCount = tracks.length,
      npAction = $('#npAction'),
      npTitle = $('#npTitle'),
      audio = $('#audio1').on('play', function () {
        playing = true;
        npAction.text('Now Playing...');
      }).on('pause', function () {
        playing = false;
        npAction.text('Paused...');
      }).on('ended', function () {
        npAction.text('Paused...');
        if ((index + 1) < trackCount) {
          index++;
          loadTrack(index);
          audio.play();
        } else {
          audio.pause();
          index = 0;
          loadTrack(index);
        }
      }).get(0),
      btnPrev = $('#btnPrev').on('click', function () {
        if ((index - 1) > -1) {
          index--;
          loadTrack(index);
          if (playing) {
            audio.play();
          }
        } else {
          audio.pause();
          index = 0;
          loadTrack(index);
        }
      }),
      btnNext = $('#btnNext').on('click', function () {
        if ((index + 1) < trackCount) {
          index++;
          loadTrack(index);
          if (playing) {
            audio.play();
          }
        } else {
          audio.pause();
          index = 0;
          loadTrack(index);
        }
      }),
      li = $('#plList li').on('click', function () {
        var id = parseInt($(this).index());
        if (id !== index) {
          playTrack(id);
        }
      }),
      loadTrack = function (id) {
        $('.plSel').removeClass('plSel');
        $('#plList li:eq(' + id + ')').addClass('plSel');
        npTitle.text(tracks[id].name);
        index = id;
        audio.src = mediaPath + tracks[id].file + extension;
      },
      playTrack = function (id) {
        loadTrack(id);
        audio.play();
      };
    extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
    loadTrack(index);
  }
});

// initialize plyr
plyr.setup($('#audio1'), {});

</script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.min.js"></script>
		
		<script src="js/lightslider.min.js"></script>
		<script type="text/javascript" src="node_modules/recursive-readdir/index.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
<script type="text/javascript" src="js/music.js"></script>
		<script src="http://code.jquery.com/jquery-1.8.3.js"></script>   <!-- jQuery -->
		<script src="https://www.youtube.com/iframe_api"></script> 

		<script type="text/javascript" src="js/player.js"></script>

		<script src="https://js.cx/script/jquery.documentReady.js"></script>

		<script type="text/javascript" src=""></script>
		<script defer type="text/javascript" src="js/main.js"></script>

		


		<script>
 			$(function() {

  			document.getElementById('logoID').style.left="100";

 			});

 			//hide menu after click on fader

		</script>

		 




	</body>
</html>		



<?php
/*
php5 class (will not work in php4)
for detecting bitrate and duration of regular mp3 files (not VBR files)
*/

//-----------------------------------------------------------------------------
class mp3file
{
  protected $block;
  protected $blockpos;
  protected $blockmax;
  protected $blocksize;
  protected $fd;
  protected $bitpos;
  protected $mp3data;
  public function __construct($filename)
  {
    $this->powarr = array(0=>1,1=>2,2=>4,3=>8,4=>16,5=>32,6=>64,7=>128);
    $this->blockmax= 1024;
    
    $this->mp3data = array();
    $this->mp3data['Filesize'] = filesize($filename);

    $this->fd = fopen($filename,'rb');
    $this->prefetchblock();
    $this->readmp3frame();
  }
  public function __destruct()
  {
    fclose($this->fd);
  }
  //-------------------
  public function get_metadata()
  {
    return $this->mp3data;
  }
  protected function readmp3frame()
  {
    $iscbrmp3=true;
    if ($this->startswithid3())
      $this->skipid3tag();
    else if ($this->containsvbrxing())
    {
      $this->mp3data['Encoding'] = 'VBR';
      $iscbrmp3=false;
    }
    else if ($this->startswithpk())
    {
      $this->mp3data['Encoding'] = 'Unknown';
      $iscbrmp3=false;
    }
  
    if ($iscbrmp3)
    {
      $i = 0;
      $max=5000;
      //look in 5000 bytes... 
      //the largest framesize is 4609bytes(256kbps@8000Hz mp3)
      for($i=0; $i<$max; $i++)
      {
        //looking for 1111 1111 111 (frame synchronization bits)        
        if ($this->getnextbyte()==0xFF)
          if ($this->getnextbit() && $this->getnextbit() && $this->getnextbit())
            break;
      }
      if ($i==$max)
        $iscbrmp3=false;
    }
  
    if ($iscbrmp3)
    {
      $this->mp3data['Encoding'     ] = 'CBR';
      $this->mp3data['MPEG version'   ] = $this->getnextbits(2);
      $this->mp3data['Layer Description'] = $this->getnextbits(2);
      $this->mp3data['Protection Bit'  ] = $this->getnextbits(1);
      $this->mp3data['Bitrate Index'  ] = $this->getnextbits(4);
      $this->mp3data['Sampling Freq Idx'] = $this->getnextbits(2);
      $this->mp3data['Padding Bit'   ] = $this->getnextbits(1);
      $this->mp3data['Private Bit'   ] = $this->getnextbits(1);
      $this->mp3data['Channel Mode'   ] = $this->getnextbits(2);
      $this->mp3data['Mode Extension'  ] = $this->getnextbits(2);
      $this->mp3data['Copyright'    ] = $this->getnextbits(1);
      $this->mp3data['Original Media'  ] = $this->getnextbits(1);
      $this->mp3data['Emphasis'     ] = $this->getnextbits(1);
      $this->mp3data['Bitrate'     ] = mp3file::bitratelookup($this->mp3data);
      $this->mp3data['Sampling Rate'  ] = mp3file::samplelookup($this->mp3data);
      $this->mp3data['Frame Size'    ] = mp3file::getframesize($this->mp3data);
      $this->mp3data['Length'      ] = mp3file::getduration($this->mp3data,$this->tell2());
      $this->mp3data['Length mm:ss'   ] = mp3file::seconds_to_mmss($this->mp3data['Length']);
      
      if ($this->mp3data['Bitrate'   ]=='bad'   ||
        $this->mp3data['Bitrate'   ]=='free'  ||
        $this->mp3data['Sampling Rate']=='unknown' ||
        $this->mp3data['Frame Size'  ]=='unknown' ||
        $this->mp3data['Length'   ]=='unknown')
      $this->mp3data = array('Filesize'=>$this->mp3data['Filesize'], 'Encoding'=>'Unknown');
    }
    else
    {
      if(!isset($this->mp3data['Encoding']))
        $this->mp3data['Encoding'] = 'Unknown';
    }
  }
  protected function tell()
  {
    return ftell($this->fd);
  }
  protected function tell2()
  {
    return ftell($this->fd)-$this->blockmax +$this->blockpos-1;
  }
  protected function startswithid3()
  {
    return ($this->block[1]==73 && //I
        $this->block[2]==68 && //D
        $this->block[3]==51); //3
  }
  protected function startswithpk()
  {
    return ($this->block[1]==80 && //P
        $this->block[2]==75); //K
  }
  protected function containsvbrxing()
  {
    //echo "<!--".$this->block[37]." ".$this->block[38]."-->";
    //echo "<!--".$this->block[39]." ".$this->block[40]."-->";
    return(
        ($this->block[37]==88 && //X 0x58
        $this->block[38]==105 && //i 0x69
        $this->block[39]==110 && //n 0x6E
        $this->block[40]==103)  //g 0x67
/*        || 
        ($this->block[21]==88 && //X 0x58
        $this->block[22]==105 && //i 0x69
        $this->block[23]==110 && //n 0x6E
        $this->block[24]==103)  //g 0x67*/
       );  

  } 
  protected function debugbytes()
  {
    for($j=0; $j<10; $j++)
    {
      for($i=0; $i<8; $i++)
      {
        if ($i==4) echo " ";
        echo $this->getnextbit();
      }
      echo "<BR>";
    }
  }
  protected function prefetchblock()
  {
    $block = fread($this->fd, $this->blockmax);
    $this->blocksize = strlen($block);
    $this->block = unpack("C*", $block);
    $this->blockpos=0;
  }
  protected function skipid3tag()
  {
    $bits=$this->getnextbits(24);//ID3
    $bits.=$this->getnextbits(24);//v.v flags

    //3 bytes 1 version byte 2 byte flags
    $arr = array();
    $arr['ID3v2 Major version'] = bindec(substr($bits,24,8));
    $arr['ID3v2 Minor version'] = bindec(substr($bits,32,8));
    $arr['ID3v2 flags'    ] = bindec(substr($bits,40,8));
    if (substr($bits,40,1)) $arr['Unsynchronisation']=true;
    if (substr($bits,41,1)) $arr['Extended header']=true;
    if (substr($bits,42,1)) $arr['Experimental indicator']=true;
    if (substr($bits,43,1)) $arr['Footer present']=true;

    $size = "";
    for($i=0; $i<4; $i++)
    {
      $this->getnextbit();//skip this bit, should be 0
      $size.= $this->getnextbits(7);
    }

    $arr['ID3v2 Tags Size']=bindec($size);//now the size is in bytes;
    if ($arr['ID3v2 Tags Size'] - $this->blockmax>0)
    {
      fseek($this->fd, $arr['ID3v2 Tags Size']+10 );
      $this->prefetchblock();
      if (isset($arr['Footer present']) && $arr['Footer present'])
      {
        for($i=0; $i<10; $i++)
          $this->getnextbyte();//10 footer bytes
      }
    }
    else
    {
      for($i=0; $i<$arr['ID3v2 Tags Size']; $i++)
        $this->getnextbyte();
    }
  }

  protected function getnextbit()
  {
    if ($this->bitpos==8)
      return false;

    $b=0;
    $whichbit = 7-$this->bitpos;
    $mult = $this->powarr[$whichbit]; //$mult = pow(2,7-$this->pos);
    $b = $this->block[$this->blockpos+1] & $mult;
    $b = $b >> $whichbit;
    $this->bitpos++;

    if ($this->bitpos==8)
    {
      $this->blockpos++;
        
      if ($this->blockpos==$this->blockmax) //end of block reached
      {
        $this->prefetchblock();
      }
      else if ($this->blockpos==$this->blocksize) 
      {//end of short block reached (shorter than blockmax)
        return;//eof 
      }
      
      $this->bitpos=0;
    }
    return $b;
  }
  protected function getnextbits($n=1)
  {
    $b="";
    for($i=0; $i<$n; $i++)
      $b.=$this->getnextbit();
    return $b;
  }
  protected function getnextbyte()
  {
    if ($this->blockpos>=$this->blocksize)
      return;

    $this->bitpos=0;
    $b=$this->block[$this->blockpos+1];
    $this->blockpos++;
    return $b;
  }
  //-----------------------------------------------------------------------------
  public static function is_layer1(&$mp3) { return ($mp3['Layer Description']=='11'); }
  public static function is_layer2(&$mp3) { return ($mp3['Layer Description']=='10'); }
  public static function is_layer3(&$mp3) { return ($mp3['Layer Description']=='01'); }
  public static function is_mpeg10(&$mp3) { return ($mp3['MPEG version']=='11'); }
  public static function is_mpeg20(&$mp3) { return ($mp3['MPEG version']=='10'); }
  public static function is_mpeg25(&$mp3) { return ($mp3['MPEG version']=='00'); }
  public static function is_mpeg20or25(&$mp3) { return ($mp3['MPEG version']{1}=='0'); }
  //-----------------------------------------------------------------------------
  public static function bitratelookup(&$mp3)
  {
    //bits        V1,L1 V1,L2 V1,L3 V2,L1 V2,L2&L3
    $array = array();
    $array['0000']=array('free','free','free','free','free');
    $array['0001']=array( '32', '32', '32', '32',  '8');
    $array['0010']=array( '64', '48', '40', '48', '16');
    $array['0011']=array( '96', '56', '48', '56', '24');
    $array['0100']=array( '128', '64', '56', '64', '32');
    $array['0101']=array( '160', '80', '64', '80', '40');
    $array['0110']=array( '192', '96', '80', '96', '48');
    $array['0111']=array( '224', '112', '96', '112', '56');
    $array['1000']=array( '256', '128', '112', '128', '64');
    $array['1001']=array( '288', '160', '128', '144', '80');
    $array['1010']=array( '320', '192', '160', '160', '96');
    $array['1011']=array( '352', '224', '192', '176', '112');
    $array['1100']=array( '384', '256', '224', '192', '128');
    $array['1101']=array( '416', '320', '256', '224', '144');
    $array['1110']=array( '448', '384', '320', '256', '160');
    $array['1111']=array( 'bad', 'bad', 'bad', 'bad', 'bad');
    
    $whichcolumn=-1;
    if   (mp3file::is_mpeg10($mp3) && mp3file::is_layer1($mp3) )//V1,L1
      $whichcolumn=0;
    else if (mp3file::is_mpeg10($mp3) && mp3file::is_layer2($mp3) )//V1,L2
      $whichcolumn=1;
    else if (mp3file::is_mpeg10($mp3) && mp3file::is_layer3($mp3) )//V1,L3
      $whichcolumn=2;
    else if (mp3file::is_mpeg20or25($mp3) && mp3file::is_layer1($mp3) )//V2,L1
      $whichcolumn=3;
    else if (mp3file::is_mpeg20or25($mp3) && (mp3file::is_layer2($mp3) || mp3file::is_layer3($mp3)) )
      $whichcolumn=4;//V2,  L2||L3 
    
    if (isset($array[$mp3['Bitrate Index']][$whichcolumn]))
      return $array[$mp3['Bitrate Index']][$whichcolumn];
    else 
      return "bad";
  }
  //-----------------------------------------------------------------------------
  public static function samplelookup(&$mp3)
  {
    //bits        MPEG1  MPEG2  MPEG2.5
    $array = array();
    $array['00'] =array('44100','22050','11025');
    $array['01'] =array('48000','24000','12000');
    $array['10'] =array('32000','16000','8000');
    $array['11'] =array('res','res','res');
    
    $whichcolumn=-1;
    if   (mp3file::is_mpeg10($mp3))
      $whichcolumn=0;
    else if (mp3file::is_mpeg20($mp3))
      $whichcolumn=1;
    else if (mp3file::is_mpeg25($mp3))
      $whichcolumn=2;
    
    if (isset($array[$mp3['Sampling Freq Idx']][$whichcolumn]))
      return $array[$mp3['Sampling Freq Idx']][$whichcolumn];
    else 
      return 'unknown';
  }
  //-----------------------------------------------------------------------------
  public static function getframesize(&$mp3)

  {
    if ($mp3['Sampling Rate']>0)
    {
      return ceil((144 * $mp3['Bitrate']*1000)/$mp3['Sampling Rate']) + $mp3['Padding Bit'];
    }
    return 'unknown';
  }
  //-----------------------------------------------------------------------------
  public static function getduration(&$mp3,$startat)
  {
    if ($mp3['Bitrate']>0)
    {
      $KBps = ($mp3['Bitrate']*1000)/8;
      $datasize = ($mp3['Filesize'] - ($startat/8));
      $length = $datasize / $KBps;
      return sprintf("%d", $length);
    }
    return "unknown";
  }
  //-----------------------------------------------------------------------------
  public static function seconds_to_mmss($duration)
  {
    return sprintf("%d:%02d", ($duration /60), $duration %60 );
  }
}
?>