//Инициализация плеера

var playerVideoId = 'qmHVO7QniMA';

var videoPlaylist = [];


function onYouTubeIframeAPIReady() {
	  player = new YT.Player('player', {
		height: '500',
		playerVars: { 'autoplay': 0, 'controls': 0, 'showinfo': 0, 'rel': 0},
		width: '850',
		videoId: playerVideoId,
		events: {
		  'onReady': onPlayerReady,
		  'onStateChange': onPlayerStateChange
		}
  });
}

function stopPlay() {
	player.stopVideo();
}
		

// Обработчик готовность
function onPlayerReady(event) {
	var player = event.target;
	iframe = document.getElementById('player');
	setupListener(); 			  
	updateTimerDisplay();
	
	
	time_update_interval = setInterval(function () {
		updateTimerDisplay();
		updateProgressBar();
		updateProgressBarVolume();
	}, 1000);		  
}

function onPlayerStateChange(){
	var videotitle = document.getElementById("videotitle");
		videotitle.innerHTML =  player.getVideoData().title;
}

/*Слушать события*/
function setupListener (){
		loadPlaylistVideoIds();	
		updateProgressBar();
		updateProgressBarVolume();		
	
}
/*Включение фуллскрина*/
function playFullscreen (){
	player.playVideo();//won't work on mobile
			  
	  var requestFullScreen = iframe.requestFullScreen || iframe.mozRequestFullScreen || iframe.webkitRequestFullScreen;
	  if (requestFullScreen) {
		requestFullScreen.bind(iframe)();
	  }
}
/*Загрузить плейлист*/		


function loadPlaylistVideoIds() {
	player.cuePlaylist({
		'playlist': videoPlaylist,
		'listType': 'playlist',
		'index': 0,
		'startSeconds': 0,
		'suggestedQuality': 'small'
			});
}			
/*Громкость*/
function editVolume () {				

		player.setVolume('100');
}
			
/*Качество*/
function editQuality () {
	player.setPlaybackQuality('medium');			
	document.getElementById('quality').innerHTML = '480';
}
			
// Обновляем время на панельке - счетчик
function updateTimerDisplay(){
	document.getElementById('time').innerHTML = formatTime(player.getCurrentTime());
}
/*Формат времени*/
function formatTime(time){
	time = Math.round(time);
	var minutes = Math.floor(time / 60),
	seconds = time - minutes * 60;
	seconds = seconds < 10 ? '0' + seconds : seconds;
	return minutes + ":" + seconds;
}

// Обновляем прогресс
function updateProgressBar(){

	var line_width = jQuery('#line').width();
	var persent = (player.getCurrentTime() / player.getDuration());
	jQuery('.viewed').css('width', persent * line_width);
	per = persent * 100;
	//document.getElementById('fader').style.marginLeft = "50px";
	jQuery('#videoFader').css('left', per+'%');
}

/*Линия прогресса*/
function progress (event) {
				
	var line_width = jQuery('#line').width();
	// положение элемента
	var pos = jQuery('#line').offset();
	var elem_left = pos.left;	
	// положение курсора внутри элемента
	var Xinner = event.pageX - elem_left;
	var newTime = player.getDuration() * (Xinner / line_width);
	// Skip video to new time.
	player.seekTo(newTime);
}

function updateProgressBarVolume(){

	var line_width = jQuery('#volume_line').width();
	var persent = player.getVolume();
	jQuery('.volume_level').css('width', persent * (line_width/100));
	per = persent ;
	//document.getElementById('fader').style.marginLeft = "50px";
	jQuery('#volume_fader').css('left', per+'%');
}



function volume (event) {
				
	var line_width2 = jQuery('#volume_line').width();
	// положение элемента
	var pos2 = jQuery('#volume_line').offset();
	var elem_left2 = pos2.left;	
	// положение курсора внутри элемента
	var Xinner2 = event.pageX - elem_left2;
	var newVolume = 100*(Xinner2 / line_width2);
	// Skip video to new time.
	player.setVolume(newVolume);
}


// verify is IE
var isIE9 = document.addEventListener,
isIE8 = document.querySelector,
isIE7 = window.XMLHttpRequest;


var IEWIN = isIE9 && isIE8 && isIE7;


var data;

    		function httpGet(){
		
   				var xmlHttp = new XMLHttpRequest();
    			xmlHttp.onreadystatechange = function() {
    				if (this.readyState == 4 && this.status == 200) {
      					data = JSON.parse(this.responseText);
      					console.log(data)
      						
      					for (var i = 0; i < data.items.length; i++) {
								
    						var url = 'https://www.youtube.com/embed/' + data.items[i].snippet.resourceId['videoId'] + '?color=white&controls=0&fs=0&iv_load_policy=3&modestbranding=1&rel=0&showinfo=0';
    						
    						var id = data.items[i].snippet.resourceId['videoId'];

    						videoPlaylist.push(id);



    						var alt = data.items[i].snippet.title;

    						var standart_thumb = data.items[i].snippet.thumbnails.standard.url;
    						
    						var title = data.items[i].snippet.title;
    						title = title.replace(new RegExp("\"", 'g'),"\'");

    						var playlistview = document.getElementById("player_list");

    						var playlistitem = document.createElement('div');
    						playlistitem.className = "row playlist_item";
    						//playlistitem.style.height = "40%";

    						var playlistthumb = document.createElement('div');
    						playlistthumb.className = "col-xl-12 thumb";

    						var thumb = IEWIN ? new Image() : document.createElement('img');
    						thumb.src = standart_thumb;



    						if ( alt != null ) 
    							thumb.alt = alt;
    						if ( title != null ) 
    							thumb.title = title;

    						thumb.setAttribute("onclick", "changeVideo(\"" + url +"\", \"" + i + "\",\"" + id +"\", \"" + title +"\")");

    						playlistthumb.appendChild(thumb);
    						playlistitem.appendChild(playlistthumb);
    						playlistview.appendChild(playlistitem);

    						console.log(playlistview);
    									
/*    						document.write("<div class=\"row\" style=\"margin-top: 4%;\">");    		 	
    		 				document.write("<div class=\"col-xl-12 thumb\">");
    		 				document.write('<img src=\"' + strandrt_thumb + '\" onclick=\"changeVideo\(\'' + url +'\', \'' + title + '\'\)\" ></img>');
							document.write("</div>");    		 	
    		 				document.write("</div>");*/
						};


					};
  				};
    			xmlHttp.open( "GET", "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=PLMvGOxm0TXEVuCk3DfyIrxZohzukOOm14&key=AIzaSyCBT5LUzqeu2_brxGNn-gc8un5Bk3STnzU", false ); // false for synchronous request

    			xmlHttp.send();
			};

    		new httpGet();

    		console.log(data);	

    			//Change video from slider
	function changeVideo(url, i, id, title){
		console.log("change video");

		playerVideoId = id;
		player.playVideoAt(i);
		//player.loadVideoById(id);

		var mainvideo = document.getElementById("mainvideo");
		var videotitle = document.getElementById("videotitle");
		videotitle.innerHTML = title;

	}