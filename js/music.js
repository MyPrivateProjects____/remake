jQuery(function ($) {
  'use strict'
  var supportsAudio = !!document.createElement('audio').canPlayType;
  if (supportsAudio) {
    var index = 0,
      playing = false,
      mediaPath = '/',
      extension = '',
      tracks = [
      <?php
              $dirname = "mp3/";
              $files = scandir($dirname);
              $ignore = array(".", "..", ".DS_Store");
              $a = 1; 
              foreach($files as $curfile){
                if(!in_array($curfile, $ignore)) {

                  echo '{ "track": '.$a.', "name": "'.substr($curfile, 0, strlen($curfile) -4). '", "duration": "25:28", "file": "mp3/'.substr($curfile, 0, strlen($curfile) -4).'" }, ';
                  $a = $a + 1;
                }
              } 
            ?>
      {
        "track": 0,
        "name": "",
        "duration": "",
        "file": "JLS_ATI"
      }],
      buildPlaylist = $(tracks).each(function(key, value) {
        var trackNumber = value.track,
          trackName = value.name,
          trackDuration = value.duration;
        if (trackNumber.toString().length === 1) {
          trackNumber = '0' + trackNumber;
        }
        $('#plList').append('<li><div class="plItem"><span class="plNum">' + trackNumber + '.</span><span class="plTitle">' + trackName + '</span><span class="plLength">' + trackDuration + '</span></div></li>');
      }),
      trackCount = tracks.length,
      npAction = $('#npAction'),
      npTitle = $('#npTitle'),
      audio = $('#audio1').on('play', function () {
        playing = true;
        npAction.text('Now Playing...');
      }).on('pause', function () {
        playing = false;
        npAction.text('Paused...');
      }).on('ended', function () {
        npAction.text('Paused...');
        if ((index + 1) < trackCount) {
          index++;
          loadTrack(index);
          audio.play();
        } else {
          audio.pause();
          index = 0;
          loadTrack(index);
        }
      }).get(0),
      btnPrev = $('#btnPrev').on('click', function () {
        if ((index - 1) > -1) {
          index--;
          loadTrack(index);
          if (playing) {
            audio.play();
          }
        } else {
          audio.pause();
          index = 0;
          loadTrack(index);
        }
      }),
      btnNext = $('#btnNext').on('click', function () {
        if ((index + 1) < trackCount) {
          index++;
          loadTrack(index);
          if (playing) {
            audio.play();
          }
        } else {
          audio.pause();
          index = 0;
          loadTrack(index);
        }
      }),
      li = $('#plList li').on('click', function () {
        var id = parseInt($(this).index());
        if (id !== index) {
          playTrack(id);
        }
      }),
      loadTrack = function (id) {
        $('.plSel').removeClass('plSel');
        $('#plList li:eq(' + id + ')').addClass('plSel');
        npTitle.text(tracks[id].name);
        index = id;
        audio.src = mediaPath + tracks[id].file + extension;
      },
      playTrack = function (id) {
        loadTrack(id);
        audio.play();
      };
    extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
    loadTrack(index);
  }
});

// initialize plyr
plyr.setup($('#audio1'), {});